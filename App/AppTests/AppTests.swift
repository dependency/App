//
//  AppTests.swift
//  AppTests
//
//  Created by Mohsen Ramezanpoor on 01/02/2016.
//  Copyright © 2016 RGB. All rights reserved.
//

import XCTest
import Tester
@testable import App

class AppTests: XCTestCase {
    
    func testExample() {
        assert(quadruple(4) == 16)
    }
    
}
