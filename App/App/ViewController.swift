//
//  ViewController.swift
//  App
//
//  Created by Mohsen Ramezanpoor on 01/02/2016.
//  Copyright © 2016 RGB. All rights reserved.
//

import UIKit
import Content
import Utility

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Use Content alone
        print(generate())
        
        // Use Utility alone
        print(describe(17))
        
        // Use Content calling Trig
        print(generateTrig())

        // Use Content calling Utility
        print(generateDescription())
        
    }

}

